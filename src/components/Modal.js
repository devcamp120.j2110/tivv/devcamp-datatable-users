
import { Box, Modal, TextField } from '@mui/material'

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
function ModalDetail({ open, setOpen, value }) {
    const handleClose = () => setOpen(false);
    return (
        // <Button onClick={handleOpen}>Open modal</Button>
        <Modal open={open}
            onClose={handleClose}
            aria-labelledby="modal-title"
            aria-describedby="modal-description">

            <Box sx={style}>
                <TextField sx={{pb:2}} label="Id" variant="outlined" fullWidth value={value.id}/>
                <TextField sx={{pb:2}} label="First name" variant="outlined" fullWidth value={value.firstname}/>
                <TextField sx={{pb:2}} label="Last name" variant="outlined" fullWidth value={value.lastname}/>
                <TextField sx={{pb:2}} label="Country" variant="outlined" fullWidth value={value.country}/>
                <TextField sx={{pb:2}} label="Subject" variant="outlined" fullWidth value={value.subject}/>
                <TextField sx={{pb:2}} label="Registry Status" variant="outlined" fullWidth value={value.registerStatus}/>
            </Box>

        </Modal>
    )
}
export default ModalDetail;