import { useEffect, useState } from "react";
import { Table, TableContainer, TableHead, TableRow, TableCell, TableBody, Container, Grid, Paper, Button, Pagination } from "@mui/material";
import { Box, FormControl, InputLabel, NativeSelect } from '@mui/material'
import ModalDetail from "./Modal";

function UserData() {
    const [data, setData] = useState([]);
    const [openModal, setOpenModal] = useState(false);
    const [post, setPost] = useState({
        id: null,
        firstname: null,
        lastname: null,
        country: null,
        subject: null,
        registryStatus: null
    });
    const [limitRowPerPage, setLimitRowPerPage] = useState(10);
    const [selectPage, setSelectPage] = useState(1);
    const [totalPage, setTotalPage] = useState(1);

    const getData = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-register-java-api/users");
        const data = await response.json();
        return data;
    }
    useEffect(() => {
        getData()
            .then((data) => {
                // console.log(data);
                setTotalPage(Math.ceil(data.length / limitRowPerPage));
                setData(data.slice(selectPage * limitRowPerPage - limitRowPerPage, selectPage * limitRowPerPage));
            }).catch((error) => {
                console.log(error);
            })
    }, [selectPage, limitRowPerPage])
    const onRowClick = (posts) => {
        console.log(posts);
        setOpenModal(true);
        setPost(posts);
    }
    const changePage = (event, value) => {
        setSelectPage(value);
    }
    const handleChange = (event) => {
        // console.log(event.target.value);
        setLimitRowPerPage(event.target.value);
    }
    return (
        <div>

            <h1 align='center'>Danh sách đăng ký</h1>
            <Grid container item sm={12} lg={12} md={12}>
                <Container>
                    <Box sx={{ minWidth: 120, py:4 }}>
                        <FormControl fullWidth>
                            <InputLabel variant="standard" htmlFor="uncontrolled-native">
                                NoRow Per Page:
                            </InputLabel>
                            <NativeSelect
                                defaultValue={10} onChange={handleChange}
                            >
                                <option value={5}>5</option>
                                <option value={10}>10</option>
                                <option value={25}>25</option>
                                <option value={50}>50</option>
                            </NativeSelect>
                        </FormControl>
                    </Box>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center" sx={{ fontWeight: 'bold' }}>Mã người dùng</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: 'bold' }}>Firstname</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: 'bold' }}>Lastname</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: 'bold' }}>Country</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: 'bold' }}>Subject</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: 'bold' }}>Customer Type</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: 'bold' }}>Registry Status</TableCell>
                                    <TableCell align="center" sx={{ fontWeight: 'bold' }}>Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.map((row, index) => (
                                    <TableRow
                                        key={index}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row" align="center">
                                            {row.id}
                                        </TableCell>
                                        <TableCell align="left">{row.firstname}</TableCell>
                                        <TableCell align="left">{row.lastname}</TableCell>
                                        <TableCell align="left">{row.country}</TableCell>
                                        <TableCell align="left">{row.subject}</TableCell>
                                        <TableCell align="left">{row.customerType}</TableCell>
                                        <TableCell align="left">{row.registerStatus}</TableCell>
                                        <TableCell align="center">
                                            <Button color="success" variant="contained" onClick={() => onRowClick(row)}>Chi tiết</Button>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <Pagination sx={{ py: 2 }} count={totalPage} color="primary" defaultPage={selectPage} onChange={changePage} />
                </Container>
            </Grid>
            <ModalDetail open={openModal} setOpen={setOpenModal} value={post} />
        </div>
    )
}
export default UserData;